#! /usr/bin/perl -T

use lib '.';
use strict;
use warnings;
use CGI;
use Util;
use Convert::Base32;
use MIME::Base64;
use GD::Barcode::QRcode;

# Global settings...
my %config = &Util::ReadConfigFile;

my $query = new CGI;

my $random_id = $query->param('id');
$random_id =~ /^((\d+)-([a-f0-9]+))$/;
$random_id = $1;
my $timestamp = $2;

if ($timestamp + 1800 < time()) {
	&Util::HTMLError("Timestamp too old, please request a new seed");
}

my $filename = $config{totpticketdirectory} . "/" . $random_id;
open(my $fh, "<", $filename) or &Util::HTMLError("TOTP seed file not found or permission denied: $! ; $filename");
my $hex_seed = <$fh>;
chomp $hex_seed;
my $accountname = <$fh>;
chomp $accountname;
my $seed = encode_base32(pack('H*', $hex_seed));
close $fh;
#unlink $filename;

my $totpurl = "otpauth://totp/Debian:$accountname?secret=$seed&issuer=Debian";
my $totppng = "data:image/png;base64, " .
	encode_base64(GD::Barcode::QRcode->new($totpurl,
					       { ModuleSize => 10 })->plot->png);

&Util::HTMLSendHeader;
open (F, "<", "fetch-totp-seed.html") || &Util::HTMLError($!);
while (<F>) {
    s/~totppng~/$totppng/g;
    s/~totpseed~/$seed/g;
    print;
  }
  close F;

# fill out HTML template with QR code (inline svg/png?)
# self-link with png link to avoid changing content-security-policy (change it back)

exit 0;
